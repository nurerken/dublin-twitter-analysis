#!/usr/bin/python
# -*- coding: utf-8 -*-
# team 14, Dublin city


import boto, time, paramiko, os
from boto.ec2.connection import EC2Connection
from boto.ec2.regioninfo import *


def main():
    # Establishing a connection with nectar data center (OpenStack)
    connection = establishConnection()
 
    # Creating instances
    Kibana  = connection.run_instances(     image_id= 'ami-000022b3', 
                                            key_name='team14key',
                                            security_groups=['team14_Security_GroupDB', 'team14_Security_Group', 'ElasticSearch'],
                                            instance_type='m1.medium', 
                                            placement='pawsey')

    time.sleep(30)
 

    harvester1  = connection.run_instances(   'ami-000022b3', 
                                            key_name='team14key',
                                            security_groups=['team14_Security_GroupDB', 'team14_Security_Group', 'ElasticSearch'],
                                            instance_type='m1.small', 
                                            placement='pawsey')
  
    harvester2  = connection.run_instances(   image_id= 'ami-000022b3', 
                                            key_name='team14key',
                                            security_groups=['team14_Security_GroupDB', 'team14_Security_Group', 'ElasticSearch'],
                                            instance_type='m1.small', 
                                            placement='pawsey')

    harvester3  = connection.run_instances(   image_id= 'ami-000022b3', 
                                            key_name='team14key',
                                            security_groups=['team14_Security_GroupDB', 'team14_Security_Group', 'ElasticSearch'],
                                            instance_type='m1.small', 
                                            placement='pawsey')

    harvester4  = connection.run_instances(   image_id= 'ami-000022b3', 
                                            key_name='team14key',
                                            security_groups=['team14_Security_GroupDB', 'team14_Security_Group', 'ElasticSearch'],
                                            instance_type='m1.small', 
                                            placement='pawsey')

    harvester5  = connection.run_instances(   image_id= 'ami-000022b3', 
                                            key_name='team14key',
                                            security_groups=['team14_Security_GroupDB', 'team14_Security_Group', 'ElasticSearch'],
                                            instance_type='m1.small', 
                                            placement='pawsey')


    


       
# A function to establish a connection with NeCTAR (OpenStack)
def establishConnection():
    region = RegionInfo(name="NeCTAR", endpoint="nova.rc.nectar.org.au")
    connection = boto.connect_ec2(
                    aws_access_key_id='c7e25b556adb45a08885523d51872f00',
                    aws_secret_access_key='30f33c3d0e914758aaa22abd48d44b82',
                    is_secure=True,
                    region=region,
                    validate_certs=False,
                    port=8773,
                    path="/services/Cloud")
    return connection



if __name__ == "__main__":
    main()
