#!/usr/bin/python
# -*- coding: utf-8 -*-
# team 14, Dublin city

import boto, time, paramiko, os
from boto.ec2.connection import EC2Connection
from boto.ec2.regioninfo import *


def main():
    # Establishing a connection with nectar data center (OpenStack)
    connection = establishConnection()
    # Creating 3 team security groups
    # The first one will be used for the instances containing couchDB
    team14_SecurityGroupDB = connection.create_security_group('team14_Security_GroupDB', 'Team 14 Defult CouchDB instances Security Group')
    # Adding three rules to this security group (http, ssh, couchDB port 5984)
    team14_SecurityGroupDB.authorize('tcp', 8080, 8080, '0.0.0.0/0')
    team14_SecurityGroupDB.authorize('tcp', 22, 22, '0.0.0.0/0')
    team14_SecurityGroupDB.authorize('tcp', 5984, 5984, '0.0.0.0/0')
    
    #the second one will be used for the instances not containing CouchDB
    team14_SecurityGroup = connection.create_security_group('team14_Security_Group', 'Team 14 Defult Security Group')
    
    # Adding two rules to this security group (http, ssh)
    team14_SecurityGroup.authorize('tcp', 8080, 8080, '0.0.0.0/0')
    team14_SecurityGroup.authorize('tcp', 22, 22, '0.0.0.0/0')
    
	#the third one will be used for elasticsearch
    ElasticSearch = connection.create_security_group('ElasticSearch', 'Team 14  Security Group for ElasticSearch')
    
    # Adding two rules to this security group (http, ssh)
    ElasticSearch.authorize('tcp', 5601, 5601, '0.0.0.0/0')
    ElasticSearch.authorize('tcp', 9200, 9200, '0.0.0.0/0')
	ElasticSearch.authorize('tcp', 9300-9400, 9300-9400, '0.0.0.0/0')
	
    # Creating a key pair to be used for login the server
    team14_Key_Pair = connection.create_key_pair('team14key')
    team14_Key_Pair.save('~/Desktop')
     

 
# A function to establish a connection with NeCTAR (OpenStack)
def establishConnection():
    region = RegionInfo(name="NeCTAR", endpoint="nova.rc.nectar.org.au")
    connection = boto.connect_ec2(
                    aws_access_key_id='c7e25b556adb45a08885523d51872f00',
                    aws_secret_access_key='30f33c3d0e914758aaa22abd48d44b82',
                    is_secure=True,
                    region=region,
                    validate_certs=False,
                    port=8773,
                    path="/services/Cloud")
    return connection



if __name__ == "__main__":
    main()
