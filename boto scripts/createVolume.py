#!/usr/bin/python
# -*- coding: utf-8 -*-
# team 14, Dublin city

import boto

from boto.ec2.connection import EC2Connection
from boto.ec2.regioninfo import *

# Establishing a connection with nectar data center (OpenStack)
region = RegionInfo(name="NeCTAR", endpoint="nova.rc.nectar.org.au")
connection = boto.connect_ec2(aws_access_key_id='c7e25b556adb45a08885523d51872f00',
                    aws_secret_access_key='30f33c3d0e914758aaa22abd48d44b82',
                    is_secure=True,
                    region=region,
                    validate_certs=False,
                    port=8773,
                    path="/services/Cloud")

reservations = connection.get_only_instances()
print reservations

#Creating volumes
vol1 = connection.create_volume(100, 'pawsey', '/dev/sdg')
vol7 = connection.create_volume(50, 'pawsey', '/dev/sdg')
vol10 = connection.create_volume(50, 'pawsey', '/dev/sdg')
vol18 = connection.create_volume(100, 'pawsey', '/dev/sdg')
vol250 = connection.create_volume(100, 'pawsey', '/dev/sdg')

#Attaching volumes
connection.attach_volume (vol[0].id, reservations[1].id, "/dev/vdc", dry_run=False)
connection.attach_volume (vol[1].id, reservations[2].id, "/dev/vdc", dry_run=False)
connection.attach_volume (vol[2].id, reservations[3].id, "/dev/vdc", dry_run=False)
connection.attach_volume (vol[3].id, reservations[4].id, "/dev/vdc", dry_run=False)
connection.attach_volume (vol[4].id, reservations[5].id, "/dev/vdc", dry_run=False)
